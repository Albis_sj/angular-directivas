import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  @Input("appResaltado") nuevoColor!: string; //variable de tipo input

  constructor(private el: ElementRef) { 
    console.log('Llamado de directiva');
    el.nativeElement.style.backgroundColor = 'yellow';
  }
  
  //Quiero escuchar en este caso el evento mouse enter
  @HostListener('mouseenter') mouseEntro(){
    console.log(this.nuevoColor); //significa que la variable orange del html de directiva esta llegando aqui
    //this.el.nativeElement.style.backgroundColor = 'yellow';
    this.resaltar(this.nuevoColor || 'yellow'); //si nuevo color esta vacio va usar el amarillo, es 
  }

  //Quiero escuchar en este caso el evento mouse leave
  @HostListener ('mouseleave') mouseSAlio (){
    //this.el.nativeElement.style.backgroundColor = null;
    this.resaltar(null!)
    
  }

  private resaltar(color:string):void{
    this.el.nativeElement.style.backgroundColor = color;
  }
}
